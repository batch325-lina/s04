USE music_db2;

--
--SELECT WHERE name LIKE '%d%';
--

SELECT * FROM artists WHERE name LIKE '%D%';

--
-- SELECT WHERE LENGTH < 230;
--

SELECT * FROM songs WHERE length < 230;

--
-- SELECT album name song name and song length;
--

SELECT a.album_title, b.song_name, b.length FROM albums as a JOIN songs as b ON a.id = b.album_id;

--
-- JOIN artists and albums name LIKE = 'A%';
--

SELECT a.*, b.* FROM artists as a JOIN albums as b ON a.id = b.artist_id WHERE name LIKE 'A%';

--
-- SORT albums z-a and limit to 4
--

SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

--
-- JOIN albums and songs sort by z-a
--

SELECT a.*, b.* FROM albums as a JOIN songs as b ON a.id = b.album_id ORDER BY album_title DESC;